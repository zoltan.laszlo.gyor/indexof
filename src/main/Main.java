package main;

public class Main {
	public static void main(String[] args) {
String text1 = "This is an example. This";
		
		System.out.println(text1.indexOf("i"));//It returns the index of the first occurrence of character ch in a given String.
		
		System.out.println(text1.indexOf("i", 3));//It returns the index of first occurrence of character ch in the given string after the specified index �fromIndex�. For example, if the indexOf() method is called like this str.indexOf(�A�, 20) then it would start looking for the character �A� in string str after the index 20.
		
		System.out.println(text1.indexOf("This"));//Returns the index of string str in a particular String
		
		System.out.println(text1.indexOf("This", 10));//Returns the index of string str in the given string after the specified index �fromIndex�.
	}
}
